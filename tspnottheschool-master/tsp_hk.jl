#=using LinearAlgebra
using JuMP
using GLPK
using LightGraphs
using GraphPlot=#
using Combinatorics
#=function algorithm TSP (G, n) is
    for k := 2 to n do
        C({k}, k) := d1,k
    end for

    for s := 2 to n−1 do
        for all S ⊆ {2, . . . , n}, |S| = s do
            for all k ∈ S do
                C(S, k) := minm≠k,m∈S [C(S\{k}, m) + dm,k]
            end for
        end for
    end for

    opt := mink≠1 [C({2, 3, . . . , n}, k) + dk, 1]
    return (opt)
end function=#
    #C[ ([1],1) ] = 1
    #dico[(array,entier)] = valeur
function dp_held_karp(W,n)
    C = Dict{Tuple{Array,Integer},Integer}()

    for k in collect(2:n)
        C[(Array([k]),k)]= collect(W[1,k])[1]
    end
    
    for s in 2:(n-1)
        for S in collect(combinations(collect(2:n),s))
            #println(S)
            for k in S
                #println(k)              
                Sp = [s for s in S if (s!=k)]
                #print(Sp)
                C[(S,k)] = collect(20000000)[1]
                for m in S
                    if m != k
                        if C[(S,k)] >= C[(Sp,m)] + W[m,k]
                            C[(S,k)] = C[(Sp,m)] + W[m,k]
                        end
                    end
                end
            end
            #println("")=#
            #print(S)
            #println("")
        end
    end
    opt = 2000000000
    
    for k in 2:n
        if opt >= C[(collect(2:n),k)] + W[k,1]
            opt = C[(collect(2:n),k)] + W[k,1]
        end
    end
    
    return opt
end

function getPath(W,n,opt)
    println("Valeur optimale : ",opt)
    S = collect(permutations(collect(1:n),n))
    idgood = Integer[]
    Sp = [Integer[]]
    M=Array{Integer,2}[]#unique(map(matriceAdj,S))
    #Ss = []
    if (true)
        for i in 1:length(S)
            if (sommeTour(W,S[i],opt))
                append!(idgood,i)
            end
        end

    end
    for index in idgood
        if !(matriceAdj(S[index]) in M)
            append!(M,[matriceAdj(S[index])])
            append!(Sp,[S[index]])
        end
    end
    println("Circuits optimaux : ")
    for i in 1:length(Sp)
       if length(Sp[i]) != 0
            println(Sp[i])
        end    
    end
    #print(idgood)
    return
end

function sommeTour(W,L,opt)
    somme = 0
    n = length(L)
    for i in 1:(n-1)
        somme+= W[L[i] ,L[i+1]]
    end
    somme+= W[L[n],L[1]]
    return (somme==opt)
end
function matriceAdj(L)
    n = length(L)
    Madj = zeros(Integer,n,n)
    for i in 1:(n-1)
        Madj[L[i],L[i+1]] = 1
        Madj[L[i+1],L[i]] = 1
    end
    Madj[L[n],L[1]] = 1
    Madj[L[1],L[n]] = 1
    return Madj
end
W = [
           0   8  4 15 15  3 ;
           8   0  5 15  2 15 ;
           4   5  0  6 15 15 ;
           15 15  6  0  5  3 ;
           15  2 15  5  0  4 ;
           3  15 15  3  4  0
       ]
n1=size(W,1) 
W2 = [
            0 15  0  0 15 15 15 ;
           15  0  1 15  0 15  1 ;
            0  1  0 15 15 15  1 ;
            0 15 15  0  1  1 15 ;
           15  0 15  1  0  1 15 ;
           15 15 15  1  1  0  0 ;
           15  1  1 15 15  0  0 ;
       ]
n2 = size(W2,1)

getPath(W,n1,dp_held_karp(W,n1)) 
getPath(W2,n2,dp_held_karp(W2,n2))