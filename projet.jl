using JuMP
import GLPK
import SparseArrays

struct Piece
    length::Int
    demand::Int
end

struct Data
    pieces::Vector{Piece}
    L::Float64
end


function get_data()
    data = [
        22 45
        42 38
        52 25
        53 11
        78 12
    ]
    return Data([Piece(data[i, 1], data[i, 2]) for i in 1:size(data, 1)], 100.0)
end

data = get_data()

I = length(data.pieces)
J = 15  
model = Model(GLPK.Optimizer)
@variable(model, x[1:I, 1:J] >= 0, Int)
@variable(model, λ[1:J], Bin)
@constraint(
    model,
    [j in 1:J],
    sum(data.pieces[i].length * x[i, j] for i in 1:I) <= data.L * λ[j],
)
@constraint(model, [i in 1:I], sum(x[i, j] for j in 1:J) >= data.pieces[i].demand)
@objective(model, Min, sum(λ[j] for j in 1:J))
model


function solve_pricing(data::Data, π::Vector{Float64})
    I = length(π)
    model = Model(GLPK.Optimizer)
    set_silent(model)
    @variable(model, λ[1:I] >= 0, Int)
    @constraint(model, sum(data.pieces[i].length * λ[i] for i in 1:I) <= data.L)
    @objective(model, Max, sum(π[i] * λ[i] for i in 1:I))
    optimize!(model)
    if objective_value(model) > 1
        return round.(Int, value.(λ))
    end
    return nothing
end

patterns = Vector{Int}[]
for i in 1:I
    pattern = zeros(Int, I)
    pattern[i] = floor(Int, min(data.L / data.pieces[i].length, data.pieces[i].demand))
    push!(patterns, pattern)
end
P = length(patterns)

model = Model(GLPK.Optimizer)
set_silent(model)
@variable(model, x[1:P] >= 0)
@objective(model, Min, sum(x))
@constraint(model, demand[i = 1:I], patterns[i]' * x == data.pieces[i].demand)
model

while true
    # Solve the linear relaxation
    optimize!(model)
    # Obtain a new dual vector
    println("New Iteration")
    println(" ")
    π = dual.(demand)
    println("π = ", π)
    # Solve the pricing problem
    new_pattern = solve_pricing(data, π)
    for i in 1:length(x)
    	v = ceil(Int, value(x[i]))
    	if v > 0
        	println("x[",i,"]=", value(x[i]))
    	end
    end
    println(" ")
    # Stop iterating if there is no new pattern
    if new_pattern === nothing
        break
    end
    push!(patterns, new_pattern)
    # Create a new column
    push!(x, @variable(model, lower_bound = 0))
    # Update the objective coefficients
    set_objective_coefficient(model, x[end], 1.0)
    # Update the non-zeros in the coefficient matrix
    for i in 1:I
        if new_pattern[i] > 0
            set_normalized_coefficient(demand[i], x[end], new_pattern[i])
        end
    end
end

for p in 1:length(x)
    v = ceil(Int, value(x[p]))
    if v > 0
        println(lpad(v, 2), " roll(s) of pattern $p")
    end
end



total_rolls = sum(ceil.(Int, value.(x)))

println(total_rolls)
