using JuMP
using Cbc
using IterTools


function held_karp(distMatrix, minCostDP, parent, IndexMap)
    n = size(distMatrix,1)
    sets = subsets(2:n)
    append!([],sets)
    for set in sets
        for currentVertex in 2:n
            if currentVertex in set
                continue
            end
            index = createIndex(currentVertex,set,IndexMap)
            minCost = 10000
            minPrevVertex = 1
            setCopy = set
            for prevVertex in set
                cost = distMatrix[prevVertex,currentVertex] + getCost(setCopy, prevVertex, minCostDP, IndexMap)
                if cost < minCost
                    minCost = cost
                    minPrevVertex = prevVertex
                end
            end
            if length(set) == 0
                minCost = distMatrix[1,currentVertex]
            end
            get!(minCostDP, index, minCost)
            print("added : [", index,", ", minCost, "] for ", currentVertex, " and ",set)
            get!(parent, index, minPrevVertex)
            println("--> parent is ", minPrevVertex)
        end
    end
    set = []
    for i in 2:n
        append!(set,i)
    end
    min = 10000
    prevVertex = -1
    setCopy = set
    for vertex in set
        cost = distMatrix[vertex,1] + getCost(setCopy,vertex,minCostDP,IndexMap)
        if cost < min
            min = cost
            prevVertex = vertex
        end
    end
    get!(parent, createIndex(1,set,IndexMap), prevVertex)
    printTour(parent,n,IndexMap)
    return min
end

function getCost(set, prevVertex, minCostDP, IndexMap)
    #setCopy = set
    deleteat!(set, findall(x->x==prevVertex, set))
    index = createIndex(prevVertex,set,IndexMap)
    cost = get(minCostDP, index, 100)
    insert!(set, 1, prevVertex)
    return cost
end

function createIndex(vertex, vertexSet, IndexMap)
    for (index, value) in pairs(IndexMap)
        if value == (vertex, vertexSet)
            return index
        end
    end
    i = rand(1:1000)
    while haskey(IndexMap,i)
        i = rand(1:1000)
    end
    get!(IndexMap, i, (vertex, vertexSet))
    return i
end

function printTour(parent, n, IndexMap)
    set = []
    for i in 1:n
        append!(set,i)
    end
    start = 1
    stack = []
    while true
        append!(stack,start)
        deleteat!(set, findall(x->x==start, set))
        start = get(parent,createIndex(start,set,IndexMap),1000)
        if (start == 1000)
            break
        end
    end
    for vertice in 1:length(stack)
        println("-> ",stack[vertice], " ")
    end
end

minCostDP = Dict()
parent = Dict()
IndexMap = Dict()

            



