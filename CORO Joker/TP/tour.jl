using JuMP
using Cbc

function lireGrille(fichier::String)
	# Le fichier est supposé se trouver dans le répertoire data
	# Si le fichier "./data/tour.txt" existe
	if isfile("./data/"*fichier)
		# L’ouvrir
		myFile = open("./data/"*fichier)
		# Lire toutes les lignes d’un fichier
		data = readlines(myFile) #Retourne un tableau de String
		# On retire les lignes vides et les commentaires :
		data = filter(line->length(line) > 0 && line[1] != '#', data)

		# La première ligne du fichier correspond aux nombres de cases
		# visitées dans les colonnes correspondantes
		# La deuxième ligne donne l'information sur les lignes
		column = data[1]
        line = data[2]
		dl = split(line, " ")
        dc = split(column, " ")
        n = size(dl,1)
		# On crée une grille avec une ligne et une colonne
		# supplementaires pour afficher les nombres ci-dessus dedans
        grille = fill(0, (n+1, n+1))
		for j in 1:n
			grille[1, 1 + j] = parse(Int64, dc[j])
            grille[1 + j, 1] = parse(Int64, dl[j])
		end

		# Fermer le fichier
		close(myFile)
	end
	return grille
end

# Modificatioin de la fonction de base pour pouvoir s'adapter
# aux dimensions de la grille d'enonce.
function tracerLigne(n)
	print(" ")
	nb = 4 + 6 * (n-1)
	for i in 1:nb print("-") end
	println()
end


function tour(t::Array{Int64, 2})

    # Taille de la grille
    n = size(t, 1)

    # Créer le modèle
    m = Model(Cbc.Optimizer)

    ### Fonction objectif
    @objective(m, Max, 1)

	### Variables
    # x[i, j, k] = 1 si le trait passe par l'entree k de la case (i,j)
	@variable(m, x[2:n, 2:n, 1:4], Bin) # Bin --> binaire
	# y[i,j] = 1 si le trait passe par la case (i,j)
	@variable(m, y[2:n, 2:n], Bin)

	### Contraintes
	# Contrainte de liaison des 2 variables :
	# Si le trait passe par la case (i,j) alors il doit aussi en sortir
	# Donc y[i,j] = 1 et x[i,j,k] = 1 pour 2 k différents donc la
	# somme sur k des x[i,j,k] vaudra 2 fois y[i,j]
	# Si le trait le passe pas par la case (i,j) alors tous les x[i,j,k]
	# valent 0 et alors ça reste toujours vrai.
	@constraint(m, liaison[i in 2:n, j in 2:n],
						sum(x[i,j,k] for k in 1:4) == 2 * y[i,j])

	# Contraintes de liaisons entree-sortie
	# Si le trait entre sur (i,j) ou sort de (i,j) par le nord,
	# On doit pouvoir fair correspondre l'entrée et la sortie de la
	# case adjacente correspondante
	@constraint(m, sud_nord[i in 3:n, j in 2:n], x[i,j,1] == x[i-1,j,2])
	# De meme pour les entrees-sorties ouest et est
	@constraint(m, est_ouest[i in 2:n, j in 3:n], x[i,j,3] == x[i,j-1,4])

	# Contraintes sur les bordures de la grille
	# Si on se situe sur la premiere ligne de la grille (bordure nord)
	# On est dans l'incapacité de se déplacer vers le nord
	@constraint(m, bord_nord, sum(x[2,j,1] for j in 2:n) == 0)
	# De meme pour le deplacement sud quand on est sur la bordure sud
	@constraint(m, bord_sud, sum(x[n,j,2] for j in 2:n) == 0)
	# De meme
	@constraint(m, bord_ouest, sum(x[i,2,3] for i in 2:n) == 0)
	# De meme
	@constraint(m, bord_est, sum(x[i,n,4] for i in 2:n) == 0)

	# Contraintes de satisfaction du nombre de cases visitees
	# Sur la meme ligne, on doit avoir exactement le chiffre en debut
	# de ligne en nombre de cases visitees
	@constraint(m, ligne[i in 2:n], sum(y[i,j] for j in 2:n) == t[i,1])
	# Sur la meme colonne, on doit aussi avoir exactement le chiffre en
	# debut de colonne en nombre de cases visitees
	@constraint(m, colonne[j in 2:n], sum(y[i,j] for i in 2:n) == t[1,j])

    ### Résoudre le problème
    optimize!(m)

    ### Afficher la solution

	println("Solution : ")
	# La premiere ligne : les restrictions sur les colonnes
	print("    |  ")
	for l in 2:(n-1)
		print(t[1,l], "  |  ")
	end
	println(t[1,n], "  |")

    for i in 2:n
		tracerLigne(n)
		# On ajoute pour chaque ligne, une ligne supplementaire en haut
		# pour pouvoir afficher les deplacements vers le nord
		print("    |")
		for j in 2:n
			if (JuMP.value(x[i,j,1]) > 0)
				print("  *  |")
			else
				print("     |")
			end
		end

		# En debut de chaque ligne : la restriction sur cette ligne
		println()
		print("  ",t[i,1], " | ")

		for j in 2:n
			# Si deplacement vers l'ouest il y a, on l'affiche
			if (JuMP.value(x[i,j,3]) > 0 )
				print("*")
			else
				print(" ")
			end
			# Si la case est visitee, on l'affiche
			if (JuMP.value(y[i,j]) == 1)
				print("*")
			else
				print(" ")
			end
			# Si deplacement vers l'est il y a, on l'affiche
			if (JuMP.value(x[i,j,4]) > 0 )
				print("*")
			else
				print(" ")
			end
			print(" | ")
		end
		println()
		# On ajoute pour chaque ligne, une ligne supplementaire en bas
		# pour pouvoir afficher les deplacements vers le sud
		print("    |")
		for j in 2:n
			if (JuMP.value(x[i,j,2]) > 0)
				print("  *  |")
			else
				print("     |")
			end
		end
		println()
	end
	tracerLigne(n)

end

t = lireGrille("tour.txt")
print(t)

tour(t)

