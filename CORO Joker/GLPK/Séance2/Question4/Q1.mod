param T;
param P;
param Palier{1..P};
set R;
set G;

param VolMin{R,1..T} >=0;
param VolMax{R,1..T} >=0;
param VolInit{R} >=0;
param ValEau{R} >=0;
param Apport{R,1..T} >=0;
param Delai{R};
param Orig{G} symbolic; #symbolic pour les listes de string
param Dest{G} symbolic;
param Rendement{G} >= 0;
param TurMin{G,1..T} >= 0;
param TurMax{G,1..T} >= 0;
param PrixElec{1..T} >= 0;


#variables 
var vTurb{G,1..T} >= 0;
var vVolume{R,0..T} >= 0; #niveau a la fin du pas de temps
var obj; 
var EtatTurb{g in G, t in 1..T,p in 1..P} binary;

#objectif
maximize z : obj;
subject to c1: obj = sum{r in R} ValEau[r] * vVolume[r,T] + sum{t in 1..T, g in G} (PrixElec[t] * Rendement[g] * vTurb[g,t]);


#contraintes
subject to TurbinageMin{g in G, t in 1..T}: vTurb[g,t] >= TurMin[g,t];
subject to TurbinageMax{g in G, t in 1..T}: vTurb[g,t] <= TurMax[g,t];

subject to VolInfRes{r in R, t in 1..T}: vVolume[r,t] >= VolMin[r,t];
subject to VolSupRes{r in R, t in 1..T}: vVolume[r,t] <= VolMax[r,t];
subject to VolumeIni{r in R}: vVolume[r,0] = VolInit[r];

subject to VolumeEquilibre{r in R, t in 1..T}: vVolume[r,t] = vVolume[r,t-1] + Apport[r,t] + sum{g in G : Dest[g] = r and t-Delai[Orig[g]] >0} (vTurb[g,t-Delai[Orig[g]]]) - sum{g in G : Orig[g] = r} (vTurb[g,t]);
subject to Ct{g in G, t in 1..T} : sum{p in 1..P} EtatTurb[g,t,p] = 1;
subject to Ct2{g in G, t in 1..T}: vTurb[g,t] = sum{p in 1..P} Palier[p]*EtatTurb[g,t,p];


solve;

display vTurb;
display vVolume;
display obj;

end;
