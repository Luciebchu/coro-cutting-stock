# STSP using the Held and Karp algorithm

Julia module for the Symmetric Travelling Salesman Problem using the Held and Kard relaxation.

Our project is presented in the from of a Julia Jupyter notebook.

# Viewing the project

## General presentation of notebooks

Notebooks are a user-friendly way of mixing code with printed results and markdown/latex writing.

A notebook is cut in blocks containing either code, printed results or markdown/latex. Each block can be run separately, and all the functions/variables declared in it are stored in the current session.

It is important to note than blocks need preferably be executed from the top down, because the variables and function calls used in a block might depend on previous blocks.

![Preview of our notebook](images/nb.png)

## Visualizing our notebook

The simplest way is to go to the [nbviewer](https://nbviewer.jupyter.org/) website and type 

```
https://signolle.iiens.net/files/heldAndKarp.ipynb
```

in the search bar.

![Screenshot of nbviewer](images/nbviewer.png)

Alternatively, you can open the notebook in a notebook editor. For information about installing such an editor, you can go to either the [Jupyter installation page](https://jupyter.org/install) or the [Pluto installation page](https://plutojl.org/#lets-do-it). Note that in order to run the blocks in the notebook rather than just seeing them, you will need to [download and install Julia > 1.4.2](https://julialang.org/downloads/).

## Converting to a classic Julia script

If you just want to see/run the code without the printing and block structure, you might want to [install nbconvert](https://nbconvert.readthedocs.io/en/latest/install.html) and run in a shell terminal the following command :

```
jupyter nbconvert --to script heldAndKarp.ipynb
```


# Requirements for running the code

If you want to run the code, either as a notebook or a script, the following libraries are necessary : 

* Cbc v0.7.1
* Combinatorics v1.0.2
* GraphPlot v0.4.4
* JuMP v0.21.4
* LightGraphs v1.3.5

Details about package installation in Julia can be found (here)[https://docs.julialang.org/en/v1/stdlib/Pkg/]. The gist of it is :
* type `julia` in a shell terminal
* press `]`
* type `add PackageName` (PackageName can be Cbc for instance)

