var x1;
var x2;
var x3;
var z;

minimize f : z;
subject to res: z = -x1+2*x2-3*x3;
subject to c1: 1<= x1 <= 4;
subject to c2: 3<= x2 <= 6;
subject to c3: 5 <= x3 <= 10;
subject to c4: x1+2*x2+x3 = 15;

solve;


display x1,x2,x3;
display z;

end;
