using JuMP
using Cbc

function lireGrille(fichier::String)
	# Le fichier est supposé se trouver dans le répertoire data
	# Si le fichier "./data/test.txt" existe
	if isfile("./data/"*fichier)
		# L’ouvrir
		myFile = open("./data/"*fichier)
		# Lire toutes les lignes d’un fichier
		data = readlines(myFile) #Retourne un tableau de String
		# On retire les lignes vides et les commentaires :
		data = filter(line->length(line) > 0 && line[1] != '#', data)
    # On récupére les données
    # ligne
    x = data[1]
    # colonne
    y = data[2]
    # on créer notre grille
    grille = fill(0,(6,6))
    # on rempli la grille
		for i in 1:5
			grille[1, i + 1] = parse(Int64, x[i])
      grille[i + 1, 1] = parse(Int64, y[i])
		end
    # Fermer le fichier
		close(myFile)
	end
  print(grille)
	return grille
end


function plne(t::Array{Int64, 2})
  n = size(t, 1)
  m = Model(Cbc.Optimizer)

  # variables 0-1 yij qui indiquent si le trait passe par la case (i, j) ou non
  @variable(m, y[2:n, 2:n], Bin)

  # variables 0-1 xijk qui indiquent si le trait passe par l’entr´ee k de la case (i, j) : k = 1 peut d´esigner l’entr´ee “nord”, k = 2 l’entr´ee “sud”, etc...
  #      N:1
  #      |
  # 3:W--k--E:4
  #      |
  #      S:2
  @variable(m, x[2:n, 2:n, 1:4], Bin)


  # contrainte de liaison
  # On sait que si yij = 1 alors xijk = 1 pour une valeurs de k en entré ET en sortie => le nb de xijk vaut 2 *y
  @constraint(m, link[i in 2:n, j in 2:n], sum(x[i,j,k] for k in 1:4) == 2 * y[i,j])
  # si on entre dans la case (i, j) par l’entrée "nord", c’est qu’on sort de la case située au dessus par la sortie "sud" + reciproque
  @constraint(m, entreSortieNS[i in 2:n-1, j in 2:n], x[i,j,1] == x[i+1,j,2])
  # si on entre dans la case (i, j) par l’entrée "est", c’est qu’on sort de la case située au dessus par la sortie "ouest" +  reciproque
  @constraint(m, entreSortieWE[i in 2:n, j in 2:n-1], x[i,j,3] == x[i,j+1,4])
  # On bloque toutes les laisons sur les bords
  @constraint(m, horizonBord, sum(x[2,i,1] + x[n,i,2] + x[i,2,3] + x[i,n,4] for i in 2:n) == 0)
  # On fait respcter le fichier d'énoncé
  @constraint(m, nbcol[i in 2:n], sum(y[i,j] for j in 2:n) == t[i,1])
  @constraint(m, nlign[i in 2:n], sum(y[i,j] for j in 2:n) == t[i,2])

  optimize!(m)


end
#t::Array{Array{Int64,2}, 2}
function affichage(t::Array{Int64, 2})
  n = size(t, 1)
  m = Model(Cbc.Optimizer)

  # variables 0-1 yij qui indiquent si le trait passe par la case (i, j) ou non
  @variable(m, y[2:n, 2:n], Bin)

  # variables 0-1 xijk qui indiquent si le trait passe par l’entr´ee k de la case (i, j) : k = 1 peut d´esigner l’entr´ee “nord”, k = 2 l’entr´ee “sud”, etc...
  #      N:1
  #      |
  # 3:W--k--E:4
  #      |
  #      S:2
  @variable(m, x[2:n, 2:n, 1:4], Bin)


  # contrainte de liaison
  # On sait que si yij = 1 alors xijk = 1 pour une valeurs de k en entré ET en sortie => le nb de xijk vaut 2 *y
  @constraint(m, link[i in 2:n, j in 2:n], sum(x[i,j,k] for k in 1:4) == 2 * y[i,j])
  # si on entre dans la case (i, j) par l’entrée "nord", c’est qu’on sort de la case située au dessus par la sortie "sud" + reciproque
  @constraint(m, bord_SN[i in 3:n, j in 2:n], x[i,j,1] == x[i-1,j,2])
	@constraint(m, bord_WE[i in 2:n, j in 3:n], x[i,j,3] == x[i,j-1,4])
  # On bloque toutes les laisons sur les bords
  @constraint(m, horizonBord, sum(x[2,i,1] + x[n,i,2] + x[i,2,3] + x[i,n,4] for i in 2:n) == 0)
  # On fait respcter le fichier d'énoncé
  @constraint(m, ncol[i in 2:n], sum(y[i,j] for j in 2:n) == t[i,1])
  @constraint(m, nlign[j in 2:n], sum(y[i,j] for i in 2:n) == t[1,j])
  # TODO : ajoutr des contraintes pour éviter les sous boucles.
  optimize!(m)
  #print(JuMP.value.(x))

  print("     ")
	for i in 2:(n-1)
		print(t[1,i], "   ")
	end
	println(t[1,n], "   ")
  for i in 2:n
    print("   ")
    for i in 1:21
      print("_")

    end
    println(" ")
    print("   |")
    for j in 2:n
      if (value(x[i,j,1]) == 1)
        print(" * |")
      else
        print("   |")
      end
    end
    println("   ")
    print(t[i,1])
    print("  |")

    #2e l
    for j in 2:n
			if (JuMP.value(x[i,j,3]) == 1)
				print("*")
			else
				print(" ")
			end
			if (JuMP.value(y[i,j]) == 1)
				print("*")
			else
				print(" ")
			end
			if (JuMP.value(x[i,j,4]) == 1 )
				print("*")
			else
				print(" ")
			end
			print("|")
		end
    #3e l
		println()
    print("   |")
		for j in 2:n
			if (JuMP.value(x[i,j,2]) == 1)
				print(" * |")
			else
				print("   |")
			end
		end
		println()
  end

end


t = lireGrille("data.txt")
affichage(t)


